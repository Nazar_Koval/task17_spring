package com.koval.validator;

@FunctionalInterface
public interface MyValidator {
    boolean validate();
}
