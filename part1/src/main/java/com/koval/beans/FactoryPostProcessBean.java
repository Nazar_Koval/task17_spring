package com.koval.beans;

import com.koval.Application;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import static com.koval.Application.*;

public class FactoryPostProcessBean implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(@NotNull ConfigurableListableBeanFactory configurableListableBeanFactory)
            throws BeansException {
        String[] beans = configurableListableBeanFactory.getBeanDefinitionNames();
        BeanDefinition beanDefinition;
        for (String s : beans) {
            if (s.equals("beanB")) {
                beanDefinition = configurableListableBeanFactory.getBeanDefinition(s);
                Application.logger.trace(ANSI_PURPLE + "\nBeanB init method before change: " + beanDefinition.getInitMethodName() + "\n");
                beanDefinition.setInitMethodName("NewInit");
                Application.logger.trace(ANSI_PURPLE + "\nBeanB init method after change: " + beanDefinition.getInitMethodName() + "\n");
                Application.logger.trace(ANSI_RESET);
                break;
            }
        }
    }
}
