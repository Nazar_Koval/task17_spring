package com.koval.beans;

import com.koval.Application;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static com.koval.Application.*;

public class BeanE implements Empty {
    private String name = "EEE";
    private int value = -777;

    public BeanE() {
    }

    public BeanE(BeanA a) {
        this.name = a.getName();
        this.value = a.getValue();
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanE{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @PostConstruct
    public void postConstruct() {
        Application.logger.trace(ANSI_PURPLE + "\nMessage from Post Construct minor BeanE method\n");
        Application.logger.trace(ANSI_RESET);
    }

    @PreDestroy
    public void preDestroy() {
        Application.logger.trace(ANSI_RED + "\nMessage from Pre Destroy minor BeanE method\n");
        Application.logger.trace(ANSI_RESET);
    }

    @Override
    public boolean validate() {
        return false;
    }
}
