package com.koval.beans;

import com.koval.Application;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import static com.koval.Application.*;

public class PostProcessBean implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(@NotNull Object bean, String beanName) throws BeansException {
        if (bean instanceof Empty) {
            if (beanName.equals("beanD") ||
                    beanName.equals("beanB") || beanName.equals("beanC") || beanName.equals("beanF")) {
                if (!((Empty) bean).validate()) {
                    Application.logger.trace( ANSI_RED + beanName + " failed validation\n");
                } else {
                    Application.logger.trace( ANSI_GREEN + beanName + " completed validation\n");
                }
            }
        }
        Application.logger.trace(ANSI_RESET);
        return bean;
    }
}
