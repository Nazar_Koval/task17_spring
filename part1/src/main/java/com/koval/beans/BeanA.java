package com.koval.beans;

import com.koval.Application;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.koval.Application.*;

public class BeanA implements InitializingBean, DisposableBean, Empty {
    private String name = "AAA";
    private int value = 77;

    public BeanA() {
    }

    public BeanA(BeanB b, BeanC c) {
        this.name = b.getName() + c.getName();
        this.value = b.getValue() + c.getValue();
    }

    public BeanA(BeanB b, BeanD d) {
        this.name = b.getName() + d.getName();
        this.value = b.getValue() + d.getValue();
    }

    public BeanA(BeanC c, BeanD d) {
        this.name = c.getName() + d.getName();
        this.value = c.getValue() + d.getValue();
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @Override
    public void destroy() throws Exception {
        Application.logger.trace(ANSI_RED + "\nMessage from destroy minor BeanA method\n");
        Application.logger.trace(ANSI_RESET);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Application.logger.trace(ANSI_YELLOW + "\nMessage from propertiesSet minor BeanA method\n");
        Application.logger.trace(ANSI_RESET);
    }

    @Override
    public boolean validate() {
        return false;
    }
}
