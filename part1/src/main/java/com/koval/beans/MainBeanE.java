package com.koval.beans;

import com.koval.Application;
import com.koval.validator.MyValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static com.koval.Application.*;

public class MainBeanE implements Empty, MyValidator {
    private String name;
    private int value;

    public MainBeanE(BeanE e) {
        this.name = e.getName();
        this.value = e.getValue();
    }

    @Override
    public String toString() {
        return "MainBeanE{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean validate() {
        return name.length() > 0 && name.length() < 100 && value >= 0 && value <= 1000;
    }

    @PostConstruct
    public void postConstruct() {
        Application.logger.trace(ANSI_PURPLE + "\nMessage from Post Construct main BeanE method\n");
        Application.logger.trace(ANSI_RESET);
    }

    @PreDestroy
    public void preDestroy() {
        Application.logger.trace(ANSI_RED + "\nMessage from Pre Destroy main BeanE method\n");
        Application.logger.trace(ANSI_RESET);
    }
}
