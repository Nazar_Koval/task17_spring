package com.koval.beans;

import com.koval.Application;
import com.koval.validator.MyValidator;

import static com.koval.Application.*;

public class BeanC implements MyValidator, Empty {
    private String name;
    private int value;

    public BeanC(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void init() {
        Application.logger.trace(ANSI_GREEN + "\n\n~~~~~Init method from Bean C~~~~~\n");
        Application.logger.trace(ANSI_RESET);
    }

    public void destroy() {
        Application.logger.trace(ANSI_RED + "\n\n~~~~~Destroy method from Bean C~~~~~\n");
        Application.logger.trace(ANSI_RESET);
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean validate() {
        return name.length() > 0 && name.length() < 100 && value >= 0 && value <= 1000;
    }
}
