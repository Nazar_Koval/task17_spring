package com.koval.beans;

import com.koval.Application;
import com.koval.validator.MyValidator;
import org.springframework.context.annotation.PropertySource;

import static com.koval.Application.*;

@PropertySource("classpath:values.properties")
public class BeanB implements MyValidator, Empty {
    private String name;
    private int value;

    public BeanB() {

    }

    public BeanB(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public void init() {
        Application.logger.trace(ANSI_GREEN + "\n~~~~~Init method from Bean B~~~~~\n");
        Application.logger.trace(ANSI_RESET);
    }

    public void NewInit(){
        Application.logger.trace(ANSI_GREEN + "\n~~~~~New init method from Bean B~~~~~\n");
        Application.logger.trace(ANSI_RESET);
    }

    public void destroy() {
        Application.logger.trace(ANSI_RED + "\n~~~~~Destroy method from Bean B~~~~~\n");
        Application.logger.trace(ANSI_RESET);
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanB{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean validate() {
        return name.length() > 0 && name.length() < 100 && value >= 0 && value <= 1000;
    }
}
