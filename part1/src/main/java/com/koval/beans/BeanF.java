package com.koval.beans;

import com.koval.validator.MyValidator;

public class BeanF implements MyValidator, Empty {
    private String name = "FFF";
    private int value = -7;

    @Override
    public String toString() {
        return "BeanF{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean validate() {
        return name.length() > 0 && name.length() < 100 && value >= 0 && value <= 1000;
    }
}
