package com.koval.beans;

import com.koval.Application;
import com.koval.validator.MyValidator;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.koval.Application.*;

public class MainBeanA implements Empty, InitializingBean, DisposableBean, MyValidator {
    private String name;
    private int value;

    public MainBeanA(BeanA a) {
        this.name = a.getName();
        this.value = a.getValue();
    }

    @Override
    public String toString() {
        return "MainBeanA{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean validate() {
        return name.length() > 0 && name.length() < 100 && value >= 0 && value <= 1000;
    }

    @Override
    public void destroy() throws Exception {
        Application.logger.trace(ANSI_RED + "\nMessage from destroy main BeanA method\n");
        Application.logger.trace(ANSI_RESET);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Application.logger.trace(ANSI_YELLOW + "\nMessage from propertiesSet main BeanA method\n");
        Application.logger.trace(ANSI_RESET);
    }
}
