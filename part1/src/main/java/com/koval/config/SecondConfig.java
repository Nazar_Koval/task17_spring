package com.koval.config;

import com.koval.Application;
import com.koval.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

import static com.koval.Application.ANSI_CYAN;
import static com.koval.Application.ANSI_RESET;

@Configuration
public class SecondConfig {
    @Bean(name = "Main BeanE")
    public MainBeanE getMainBeanE(@Qualifier("beanEABC") BeanE getBeanE1) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~MAIN BEAN E CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new MainBeanE(getBeanE1);
    }

    @Bean(name = "beanEABC")
    public BeanE getBeanE1(@Qualifier("ABC Bean") BeanA getBeanA1) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN E1 CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanE(getBeanA1);
    }

    @Bean(name = "beanEAD")
    public BeanE getBeanE2(@Qualifier("ABD Bean") BeanA getBeanA2) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN E2 CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanE(getBeanA2);
    }

    @Bean(name = "beanEACD")
    public BeanE getBeanE3(@Qualifier("ACD Bean") BeanA getBeanA3) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN E1 CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanE(getBeanA3);
    }

    @Bean(name = "beanF")
    @Lazy
    @DependsOn(value = "beanC")
    public BeanF getBeanF() {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN F CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanF();
    }

    @Bean
    public PostProcessBean getPostProcessBean() {
        return new PostProcessBean();
    }

    @Bean
    public FactoryPostProcessBean getFactoryPostProcessBean() {
        return new FactoryPostProcessBean();
    }
}
