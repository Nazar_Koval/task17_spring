package com.koval.config;

import static com.koval.Application.*;

import com.koval.Application;
import com.koval.beans.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

@Configuration
@PropertySource("classpath:values.properties")
@Import(SecondConfig.class)
public class FirstConfig {
    @Value("${beanB.name}")
    private String bName;
    @Value("${beanB.value}")
    private int bValue;
    @Value("${beanC.name}")
    private String cName;
    @Value("${beanC.value}")
    private int cValue;
    @Value("${beanD.name}")
    private String dName;
    @Value("${beanD.value}")
    private int dValue;

    @Bean(name = "Main BeanA")
    @DependsOn(value = "beanC")
    public MainBeanA getMainBeanA(@Qualifier("ACD Bean") BeanA getBeanA3) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~MAIN BEAN A CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new MainBeanA(getBeanA3);
    }

    @Bean(name = "ABC Bean")
    public BeanA getBeanA1(BeanB b, BeanC c) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN A1 CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanA(b, c);
    }

    @Bean(name = "ABD Bean")
    public BeanA getBeanA2(BeanB b, BeanD d) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN A2 CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanA(b, d);
    }

    @Bean(name = "ACD Bean")
    public BeanA getBeanA3(BeanC c, BeanD d) {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN A3 CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanA(c, d);
    }

    @Bean(name = "beanB",
            initMethod = "init",
            destroyMethod = "destroy")
    @DependsOn(value = "beanD")
    public BeanB getBeanB() {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN B CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanB(bName, bValue);
    }

    @Bean(name = "beanC",
            initMethod = "init",
            destroyMethod = "destroy")
    @DependsOn(value = "beanB")
    public BeanC getBeanC() {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN C CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanC(cName, cValue);
    }

    @Bean(name = "beanD",
            initMethod = "init",
            destroyMethod = "destroy")
    public BeanD getBeanD() {
        Application.logger.trace(ANSI_CYAN + "\n\n~~~~~~~~~~~~~~~BEAN D CREATED~~~~~~~~~~~~~~~\n");
        Application.logger.trace(ANSI_RESET);
        return new BeanD(dName, dValue);
    }

}
