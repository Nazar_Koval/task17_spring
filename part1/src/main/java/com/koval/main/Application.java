package com.koval;

import com.koval.beans.*;
import com.koval.config.FirstConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.LinkedList;
import java.util.List;

public class Application {
    public static final Logger logger = LogManager.getLogger(Application.class);
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(FirstConfig.class);

        List<Empty> beans = new LinkedList<>();
        MainBeanA beanA = applicationContext.getBean(MainBeanA.class);
        beans.add(beanA);
        BeanB beanB = applicationContext.getBean(BeanB.class);
        beans.add(beanB);
        BeanC beanC = applicationContext.getBean(BeanC.class);
        beans.add(beanC);
        BeanD beanD = applicationContext.getBean(BeanD.class);
        beans.add(beanD);
        MainBeanE beanE = applicationContext.getBean(MainBeanE.class);
        beans.add(beanE);
        BeanF beanF = applicationContext.getBean(BeanF.class);
        beans.add(beanF);

        logger.trace("\n" + ANSI_GREEN + "\nConfiguration");
        for (String s:applicationContext.getBeanDefinitionNames()){
            System.out.println(s);
        }
        logger.trace(ANSI_RESET);

        logger.trace("\n" + ANSI_PURPLE + "\n~~~~~~~List of beans~~~~~~~\n");
        beans.forEach(System.out::println);
        logger.trace("\n");
        logger.trace(ANSI_RESET);

        applicationContext.close();
    }
}
