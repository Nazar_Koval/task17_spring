package com.koval.beans2;

import org.springframework.stereotype.Component;

@Component
public class CatAnimal {
    private String description = "Tom cat";

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "CatAnimal{" +
                "description='" + description + '\'' + '}';
    }
}
