package com.koval.beans2;

import org.springframework.stereotype.Component;

@Component
public class RoseFlower {
    private String description = "Red rose";

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "RoseFlower{" +
                "description='" + description + '\'' + '}';
    }
}
