package com.koval.beans2;

import org.springframework.stereotype.Component;

@Component
public class NarcissusFlower {
    private String description = "Beautiful narcissus";

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "NarcissusFlower{" +
                "description='" + description + '\'' + '}';
    }
}
