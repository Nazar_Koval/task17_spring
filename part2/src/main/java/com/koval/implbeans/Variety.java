package com.koval.implbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Variety {
    private List<Language> languages;

    @Autowired
    @Qualifier("Useful")
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }


    @Override
    public String toString() {
        return "Variety{" +
                "languages=" + languages + '}';
    }
}
