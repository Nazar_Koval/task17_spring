package com.koval.implbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Useful")
@Primary
@Order(Ordered.HIGHEST_PRECEDENCE)
public class FirstBean implements Language {
    @Override
    public String name() {
        return "Java";
    }

    @Override
    public String toString() {
        return name();
    }
}
