package com.koval.implbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Useful")
public class ThirdBean implements Language {
    @Override
    public String name() {
        return "C++";
    }

    @Override
    public String toString() {
        return name();
    }
}
