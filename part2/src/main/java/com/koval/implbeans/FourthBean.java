package com.koval.implbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Useless")
public class FourthBean implements Language {
    @Override
    public String name() {
        return "Pascal";
    }

    @Override
    public String toString() {
        return name();
    }
}
