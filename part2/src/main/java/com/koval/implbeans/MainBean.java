package com.koval.implbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class MainBean {
    private Language language1;

    public MainBean(Language language1) {
        this.language1 = language1;
    }

    private Language language2;

    @Autowired
    @Qualifier("secondBean")
    public void setLanguage2(Language language2) {
        this.language2 = language2;
    }

    @Autowired
    @Qualifier("thirdBean")
    private Language language3;

    @Override
    public String toString() {
        return "MainBean{" + "language1=" + language1 + ", language2=" + language2 + ", language3=" + language3 + '}';
    }
}
