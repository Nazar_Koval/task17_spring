package com.koval.implbeans;

@FunctionalInterface
public interface Language {
    String name();
}
