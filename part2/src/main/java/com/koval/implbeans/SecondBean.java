package com.koval.implbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Useful")
@Order(2)
public class SecondBean implements Language {
    @Override
    public String name() {
        return "Python";
    }

    @Override
    public String toString() {
        return name();
    }
}
