package com.koval.configurations;

import com.koval.otherbeans.InjectBeanA;
import com.koval.otherbeans.InjectBeanB;
import com.koval.otherbeans.InjectBeanC;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("bean")
public class MainBeanConfiguration {
    @Bean
    public InjectBeanA getA() {
        return new InjectBeanA();
    }

    @Bean
    public InjectBeanB getB() {
        return new InjectBeanB();
    }

    @Bean
    public InjectBeanC getC() {
        return new InjectBeanC();
    }
}
