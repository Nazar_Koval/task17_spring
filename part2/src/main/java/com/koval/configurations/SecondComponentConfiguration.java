package com.koval.configurations;

import com.koval.beans3.BeanE;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@Configuration
@ComponentScan("com.koval.beans2")
@ComponentScan(basePackages = "com.koval.beans3",
        excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = BeanE.class))
public class SecondComponentConfiguration {
}
