package com.koval.configurations;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.koval.beans1")
@ComponentScan("com.koval.implbeans")
@ComponentScan("com.koval.otherbeans")
public class FirstComponentConfiguration {
}
