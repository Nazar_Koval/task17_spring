package com.koval.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("comp")
@Import({FirstComponentConfiguration.class,SecondComponentConfiguration.class})
public class MainComponentConfiguration {
}
