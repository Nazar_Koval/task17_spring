package com.koval.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanF {
    private String name = "FFF";
    private int value = 6;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanF{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

}
