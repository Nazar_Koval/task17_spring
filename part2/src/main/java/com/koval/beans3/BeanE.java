package com.koval.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanE {
    private String name = "EEE";
    private int value = 666;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanE{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }

}
