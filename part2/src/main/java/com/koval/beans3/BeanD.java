package com.koval.beans3;

import org.springframework.stereotype.Component;

@Component
public class BeanD {
    private String name = "DDD";
    private int value = 7;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanD{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }
}
