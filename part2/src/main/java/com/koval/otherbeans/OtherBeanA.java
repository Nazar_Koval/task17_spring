package com.koval.otherbeans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class OtherBeanA {

    @Override
    public String toString() {
        String str = "AAA";
        return "OtherBeanA{" +
                "str='" + str + '\'' + '}';
    }
}
