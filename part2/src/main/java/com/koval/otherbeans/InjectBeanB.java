package com.koval.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class InjectBeanB {
    private OtherBeanB b;

    @Autowired
    @Qualifier("Setter field")
    public void setB(OtherBeanB b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "InjectBeanB{" + "b=" + b + '}';
    }
}
