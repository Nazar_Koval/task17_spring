package com.koval.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;

public class InjectBeanA {
    private OtherBeanA a;

    @Autowired
    public void setA(OtherBeanA a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "InjectBeanA{" + "a=" + a + '}';
    }
}
