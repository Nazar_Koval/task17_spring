package com.koval.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Reflect field")
public class OtherBeanC {

    @Override
    public String toString() {
        String str = "CCC";
        return "OtherBeanC{" +
                "str='" + str + '\'' + '}';
    }
}
