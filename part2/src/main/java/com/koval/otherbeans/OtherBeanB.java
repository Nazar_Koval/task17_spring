package com.koval.otherbeans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Qualifier("Setter field")
@Scope("prototype")
public class OtherBeanB {

    @Override
    public String toString() {
        String str = "BBB";
        return "OtherBeanB{" +
                "str='" + str + '\'' + '}';
    }
}
