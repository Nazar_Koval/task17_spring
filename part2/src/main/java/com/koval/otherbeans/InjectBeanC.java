package com.koval.otherbeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class InjectBeanC {

    @Autowired
    @Qualifier("Reflect field")
    private OtherBeanC c;

    @Override
    public String toString() {
        return "InjectBeanC{" + "c=" + c + '}';
    }
}
