package com.koval;

import com.koval.beans1.BeanA;
import com.koval.beans2.CatAnimal;
import com.koval.beans3.BeanD;
import com.koval.beans3.BeanE;
import com.koval.configurations.MainBeanConfiguration;
import com.koval.configurations.MainComponentConfiguration;
import com.koval.implbeans.MainBean;
import com.koval.implbeans.Variety;
import com.koval.otherbeans.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static final Logger logger = LogManager.getLogger(Application.class);
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext();
        applicationContext.getEnvironment().setActiveProfiles("bean", "comp");
        applicationContext.register(MainBeanConfiguration.class,MainComponentConfiguration.class);
        applicationContext.refresh();

        BeanD beanD = applicationContext.getBean(BeanD.class);
        logger.trace(ANSI_CYAN + beanD + ANSI_RESET);
        BeanA beanA = applicationContext.getBean(BeanA.class);
        logger.trace(ANSI_YELLOW + beanA + ANSI_RESET);
        CatAnimal animal = applicationContext.getBean(CatAnimal.class);
        logger.trace(ANSI_GREEN + animal + ANSI_RESET);
//  ERROR      BeanE beanE = applicationContext.getBean(BeanE.class);

        InjectBeanA a = applicationContext.getBean(InjectBeanA.class);
        logger.trace(ANSI_PURPLE + a + ANSI_RESET);
        InjectBeanB b = applicationContext.getBean(InjectBeanB.class);
        logger.trace(ANSI_RED + b + ANSI_RESET);
        InjectBeanC c = applicationContext.getBean(InjectBeanC.class);
        logger.trace(ANSI_YELLOW + c + ANSI_RESET);

        Variety variety = applicationContext.getBean(Variety.class);
        logger.trace(ANSI_CYAN + variety + ANSI_RESET);
        MainBean mainBean = applicationContext.getBean(MainBean.class);
        logger.trace(ANSI_RED + mainBean + ANSI_RESET);

        OtherBeanA a1 = applicationContext.getBean(OtherBeanA.class);
        logger.trace(ANSI_GREEN + "HashA1: " + a1.hashCode() + ANSI_RESET);
        OtherBeanA a2 = applicationContext.getBean(OtherBeanA.class);
        logger.trace(ANSI_GREEN + "HashA2: " + a2.hashCode() + ANSI_RESET);
        OtherBeanB b1 = applicationContext.getBean(OtherBeanB.class);
        logger.trace(ANSI_GREEN + "HashB1: " + b1.hashCode() + ANSI_RESET);
        OtherBeanB b2 = applicationContext.getBean(OtherBeanB.class);
        logger.trace(ANSI_GREEN + "HashB2: " + b2.hashCode() + ANSI_RESET);

        applicationContext.close();
    }
}
