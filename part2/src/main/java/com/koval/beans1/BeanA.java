package com.koval.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanA {
    private String name = "AAA";
    private int value = 77;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanA{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }
}
