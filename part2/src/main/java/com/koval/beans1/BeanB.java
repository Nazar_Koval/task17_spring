package com.koval.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanB {
    private String name = "BBB";
    private int value = 777;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanB{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }
}