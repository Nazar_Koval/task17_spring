package com.koval.beans1;

import org.springframework.stereotype.Component;

@Component
public class BeanC {
    private String name = "CCC";
    private int value = 66;

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "BeanC{"
                + "name='" + name + '\''
                + ", value=" + value + '}';
    }
}
